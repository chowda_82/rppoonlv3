﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class program1
    {
        static void Main(string[] args)
        {
            RandomGenerator randomGenerator = RandomGenerator.GetInstance();
            double[][] matrix = randomGenerator.NextMatrix(3, 4);
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(matrix[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
