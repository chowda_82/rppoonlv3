﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class RandomGenerator
    {
        private static RandomGenerator randomGenerator;
        private Random generator;
        private RandomGenerator()
        {
            this.generator = new Random();
        }
        public static RandomGenerator GetInstance()
        {
            if (randomGenerator == null)
            {
                randomGenerator = new RandomGenerator();
            }
            return randomGenerator;
        }
        public int NextInt()
        {
            return this.generator.Next();
        }
        public int NextInt(int upperBound)
        {
            return this.generator.Next(upperBound);
        }
        public int NextInt(int lowerBound, int upperBound)
        {
            return this.generator.Next(lowerBound, upperBound);
        }
        public double NextDouble()
        {
            return this.generator.NextDouble();
        }
        public double[][] NextMatrix(int rows, int columns)
        {
            int i, j;
            double[][] matrix = new double[rows][];
            for (i = 0; i < rows; i++)
            {
                matrix[i] = new double[columns];
            }
            for (i = 0; i < rows; i++)
            {
                for (j = 0; j < columns; j++)
                {
                    matrix[i][j] = this.generator.NextDouble();
                }
            }
            return matrix;
        }
    }
}
