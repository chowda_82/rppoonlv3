﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Dataset : Prototype
    {
        private List<List<string>> data; //list of lists of strings

        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public List<List<string>> Data
        {
            get { return data; }
            set { data = value; }
        }


        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(';');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public void Print()
        {

            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data[i].Count; j++)
                {
                    Console.Write(data[i][j]);
                }
                Console.WriteLine();
            }
        }

        public Prototype Clone()
        {
            Dataset clone = new Dataset();
            List<List<string>> data = new List<List<string>>();
            foreach (List<string> row in this.data)
            {
                List<string> rows = new List<string>();
                foreach (string item in row)
                {
                    rows.Add(item);
                }
                data.Add(rows);

            }
            clone.Data = data;
            return (Prototype)clone;
        }


    }
}
