﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("file.csv");
            dataset.Print();
            Dataset clone = new Dataset();
            clone = (Dataset)dataset.Clone();
            clone.Print();
        }
    }
}
