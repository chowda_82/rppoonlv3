﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Logger
    {
        private static Logger instance;
        private string filePath;


        public Logger(string filePath)
        {
            this.filePath = filePath;
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger("domagoj.txt");
            }
            return instance;
        }

        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public void Log(string text)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(text);
            }
        }
    }
}
