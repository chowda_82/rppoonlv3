﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class program3
    {
        static void Main(string[] args)
        {
            ConsoleNotification cNotification = new ConsoleNotification("ivan", "neki naslov", "nekakav tekst", DateTime.Now, Category.INFO, ConsoleColor.Green);
            NotificationManager manager = new NotificationManager();
            manager.Display(cNotification);
        }
    }
}
