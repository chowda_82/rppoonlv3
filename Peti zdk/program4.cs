﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class program4
    {
        static void Main(string[] args)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(notificationBuilder.Build());

            notificationBuilder.SetAuthor("chowder");
            notificationBuilder.SetText("proba obavijesti");
            notificationBuilder.SetTime(DateTime.Today);
            notificationBuilder.SetTitle("TITLE");
            notificationBuilder.SetLevel(Category.ALERT);
            notificationBuilder.SetColor(ConsoleColor.Green);
            notificationManager.Display(notificationBuilder.Build());
        }
    }
}
