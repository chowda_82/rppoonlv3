﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    interface NotificationBuilder:IBuilder
    {
        private String author;
        private String title;
        private String text;
        private DateTime timestamp;
        private Category level;
        private ConsoleColor color;
        public NotificationBuilder()
        {
            this.author = "user";
            this.title = "title";
            this.text = "text";
            this.timestamp = DateTime.Now;
            this.level = Category.INFO;
            this.color = ConsoleColor.Green;
        }

        public ConsoleNotification Build()
        {
            return new ConsoleNotification(author, title, text, timestamp, level, color);
        }

        public IBuilder SetAuthor(string author)
        {
            this.author = author;
            return this;
        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            this.level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.timestamp = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.title = title;
            return this;
        }
    }
}
